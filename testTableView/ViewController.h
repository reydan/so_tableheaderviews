#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *_first;
    NSMutableArray *_second;
    NSMutableArray *_third;
    NSMutableDictionary *_headers;
    int sectionCount;
}
@property (strong, nonatomic) UITableView *tableView;
@property (strong, nonatomic) UIButton *button;

@end
