#import "ViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface ViewController ()

@end

@implementation ViewController
@synthesize tableView;
@synthesize button;

- (void)viewDidLoad
{
    [super viewDidLoad];

    _first = [NSMutableArray arrayWithObjects: @"First 1", @"First 2", nil];
    _second = [NSMutableArray arrayWithObjects: @"Second 1", @"Second 2", nil];
    _third = [NSMutableArray arrayWithObjects: @"Third 1", @"Third 2", @"Third 3", @"Third 4", @"Third 5", nil];
    _headers = [NSMutableDictionary dictionary];
    sectionCount = 3;
    
    tableView = [[UITableView alloc] initWithFrame: [[self view] frame] style:UITableViewStylePlain];
    [tableView setDataSource: self];
    [tableView setDelegate: self];
    [[self view] addSubview: tableView];
    
    button = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [button setFrame:CGRectMake(275, 400, 40, 40)];
    [button addTarget:self action:@selector(handleTouchUp:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview: button];
    
    UIButton* button2 = [UIButton buttonWithType: UIButtonTypeRoundedRect];
    [button2 setFrame:CGRectMake(100, 400, 40, 40)];
    [button2 addTarget:self action:@selector(printHeaders:) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview: button2];
}

- (void)viewWillAppear:(BOOL)animated
{
    [tableView reloadData];
}

- (void) handleTouchUp: (UIButton *)sender
{
    NSLog(@"Touched!");
    if (sectionCount == 2)
    {
        sectionCount = 3;

        // Remove objects from the first object
        int count = _first.count;
        while (count-- > 0)
            [_third removeObjectAtIndex:0];
        
//        _third = [NSMutableArray arrayWithObjects: @"Third 1", @"Third 2", @"Third 3", @"Third 4", @"Third 5", nil];

        [tableView reloadData];

    }
    else
    {
        sectionCount = 2;

        int index = 0;
        for (id obj in _first)
            [_third insertObject:obj atIndex:index++];
        
        //        _third = [NSMutableArray arrayWithObjects: @"First 1", @"First 2", @"Third 1", @"Third 2",
        //                                                   @"Third 3", @"Third 4", @"Third 5", nil];
        
        [_headers removeObjectForKey:[NSValue valueWithNonretainedObject:_first]];
        [_headers removeObjectForKey:[NSValue valueWithNonretainedObject:_third]];
        
        
        [tableView beginUpdates];
        [tableView deleteSections: [NSIndexSet indexSetWithIndex: 0] withRowAnimation:UITableViewRowAnimationRight];
        [tableView reloadSections: [NSIndexSet indexSetWithIndex: 2] withRowAnimation:UITableViewRowAnimationAutomatic];
        [tableView endUpdates];
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return sectionCount;
}

- (NSArray*)dataArrayForSection:(NSInteger)section
{
    if (sectionCount == 3)
    {
        switch (section)
        {
            case 0: return _first;
            case 1: return _second;
            case 2: return _third;
        }
    }
    else
    {
        switch (section)
        {
            case 0: return _second;
            case 1: return _third;
        }
    }
    
    return nil;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray* dataarray = [self dataArrayForSection:section];
    if (dataarray == nil)
        return 0;
    
    return dataarray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSArray* dataarray = [self dataArrayForSection:indexPath.section];

    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"my_text_cell"];
    if (cell == nil)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"my_text_cell"];

    
    if (dataarray == nil)
        cell.textLabel.text = @"default";
    else
        cell.textLabel.text = [dataarray objectAtIndex:indexPath.row];
    
    
    return cell;
}

- (float)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 35.0f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    id dataarray = [self dataArrayForSection:section];
    UIView *header = [_headers objectForKey: [NSValue valueWithNonretainedObject:dataarray]];
    if (header == nil)
    {
        header = [[UIView alloc] init];
        
        if (dataarray == _first)
            [header setBackgroundColor: [UIColor greenColor]];
        else if (dataarray == _second)
            [header setBackgroundColor: [UIColor blueColor]];
        else if (dataarray == _third)
            [header setBackgroundColor: [UIColor redColor]];
        else
            [header setBackgroundColor: [UIColor blackColor]];

        [_headers setObject:header forKey: [NSValue valueWithNonretainedObject:dataarray]];
    }

    return header;
}

- (void)printHeaders:(id)sender
{
    for (UIView* v in _headers.allValues)
    {
        NSLog(@"Header %p frame=%@", v, NSStringFromCGRect(v.frame));
        v.alpha = 1.0f;
    }
}

@end
